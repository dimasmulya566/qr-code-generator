package com.qrcode.demo.genarate.qr.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGenarateQrCodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGenarateQrCodeApplication.class, args);
	}

}
