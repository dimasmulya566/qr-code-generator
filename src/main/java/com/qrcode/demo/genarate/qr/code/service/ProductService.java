package com.qrcode.demo.genarate.qr.code.service;

import com.google.zxing.WriterException;
import com.qrcode.demo.genarate.qr.code.dto.ProductRequest;
import com.qrcode.demo.genarate.qr.code.model.Product;
import com.qrcode.demo.genarate.qr.code.repository.ProductRepository;
import com.qrcode.demo.genarate.qr.code.util.QrCodeGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class ProductService {
    @Autowired
    private ProductRepository productRepository;


    public List<Product> getAllProduct() throws IOException, WriterException {
        List<Product> products = productRepository.findAll();
        log.info("products: {}", products);
        for (Product product : products) {
            QrCodeGenerator.qrCodeGenerator(product);
        }
        return products;
    }

    public Product getProductById(int id){
        return productRepository.findById(id).orElse(null);
    }

    public Product saveProduct(ProductRequest product){
        Product newProduct = new Product();
        newProduct.setNamaProduct(product.getNamaProduct());
        newProduct.setHarga(product.getHarga());
        newProduct.setDeskripsi(product.getDeskripsi());
        newProduct.setStok(product.getStok());
        newProduct.setQrCode(UUID.randomUUID().toString());
        return productRepository.save(newProduct);
    }


}
