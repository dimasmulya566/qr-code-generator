package com.qrcode.demo.genarate.qr.code.controller;

import com.google.zxing.WriterException;
import com.qrcode.demo.genarate.qr.code.dto.ProductRequest;
import com.qrcode.demo.genarate.qr.code.model.Product;
import com.qrcode.demo.genarate.qr.code.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/save")
    public Product saveProduct(@RequestBody ProductRequest productRequest){
        return productService.saveProduct(productRequest);
    }

    @GetMapping("/findAll")
    public List<Product> getAllProduct() throws IOException, WriterException {
        return productService.getAllProduct();
    }
}
