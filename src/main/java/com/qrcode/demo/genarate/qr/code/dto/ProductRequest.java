package com.qrcode.demo.genarate.qr.code.dto;

import lombok.Data;

@Data
public class ProductRequest {
    private String namaProduct;
    private Double harga;
    private String deskripsi;
    private int stok;
}
