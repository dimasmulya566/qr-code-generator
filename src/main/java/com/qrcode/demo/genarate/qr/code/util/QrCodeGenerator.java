package com.qrcode.demo.genarate.qr.code.util;

import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.qrcode.demo.genarate.qr.code.model.Product;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class QrCodeGenerator {

    public static void qrCodeGenerator(Product product) throws WriterException, IOException {
        String path = "E:\\QRCODE\\";
        String fileName = product.getNamaProduct()+"-QRCODE.png";
        var qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(
                "QRCode: "+product.getQrCode() + "\n" +
                "Nama Product: " + product.getNamaProduct() + "\n" +
                        "Harga: " + product.getHarga() + "\n" +
                        "Deskripsi: " + product.getDeskripsi() + "\n" +
                        "Stok: " + product.getStok(), com.google.zxing.BarcodeFormat.QR_CODE,350,350);
        Path pathFile = FileSystems.getDefault().getPath(path+fileName);
        MatrixToImageWriter.writeToPath(bitMatrix,"PNG",pathFile);


    }
}
