package com.qrcode.demo.genarate.qr.code.repository;

import com.qrcode.demo.genarate.qr.code.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {
}
